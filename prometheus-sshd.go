package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"time"

	"github.com/coreos/go-systemd/sdjournal"
	"github.com/prometheus/client_golang/prometheus"
)

// Flags
var (
	addr = flag.String("listen-address", ":9022", "The address to listen on for HTTP requests.")
)

// Prometheus metrics
var (
	sshCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "sshd_sessions",
			Help: "ssh connections status",
		},
		[]string{"status", "user", "ip"},
	)
	fingerprintUsage = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "sshd_fingerprints",
			Help: "Fingerprints used to login",
		},
		[]string{"user", "ip", "fingerprint"},
	)
)

// Regular expressions
var (
	rePasswordAccepted = regexp.MustCompile("^MESSAGE=Accepted password for (?P<user>[^ ]*) from (?P<ip>[^ ]*) port .*$")
	rePasswordFailed   = regexp.MustCompile("^MESSAGE=Failed password for (?P<user>[^ ]*) from (?P<ip>[^ ]*) port .*$")
	rePubKeyAccepted   = regexp.MustCompile("^MESSAGE=Accepted publickey for (?P<user>[^ ]*) from (?P<ip>[^ ]*) port .*: (?P<fingerprint>.*)$")
)

func processMsg(j *sdjournal.Journal) error {
	s, err := j.GetData("MESSAGE")
	if err != nil {
		return fmt.Errorf("Could not read MESSAGE from journal: %s", err)
	}
	usec, err := j.GetRealtimeUsec()
	if err != nil {
		return fmt.Errorf("GetRealtimeUsec: %s", err)
	}
	timestamp := time.Unix(0, int64(usec)*int64(time.Microsecond))
	fmt.Println(timestamp, s)

	matches := rePasswordAccepted.FindStringSubmatch(s)
	if len(matches) == 3 {
		sshCounter.WithLabelValues("accepted", matches[1], matches[2]).Inc()
	}
	matches = rePasswordFailed.FindStringSubmatch(s)
	if len(matches) == 3 {
		sshCounter.WithLabelValues("failed", matches[1], matches[2]).Inc()
	}
	matches = rePubKeyAccepted.FindStringSubmatch(s)
	if len(matches) == 4 {
		sshCounter.WithLabelValues("accepted_key", matches[1], matches[2]).Inc()
		fingerprintUsage.WithLabelValues(matches[1], matches[2], matches[3]).Inc()
	}

	return nil
}

func doFollowJournal() error {
	j, err := sdjournal.NewJournal()
	if err != nil {
		return fmt.Errorf("Could not get a journal reader: %s", err)
	}
	defer j.Close()

	if err = j.AddMatch("_SYSTEMD_UNIT=sshd.service"); err != nil {
		return err
	}
	if err = j.AddDisjunction(); err != nil {
		return err
	}
	if err = j.AddMatch("_COMM=sshd"); err != nil {
		return err
	}
	if err = j.AddDisjunction(); err != nil {
		return err
	}
	if err = j.AddMatch("SYSLOG_IDENTIFIER=sshd"); err != nil {
		return err
	}

	if err = j.SeekTail(); err != nil {
		return err
	}
	if _, err = j.Previous(); err != nil {
		return err
	}

	for {
		c, err := j.Next()
		if err != nil {
			return err
		}
		// EOF check
		if c != 0 {
			if err = processMsg(j); err != nil {
				return err
			}
		}

		// Wait for next event
		switch j.Wait(time.Duration(1) * time.Second) {
		case sdjournal.SD_JOURNAL_NOP, sdjournal.SD_JOURNAL_APPEND, sdjournal.SD_JOURNAL_INVALIDATE:
			// Do we care about the kind of event?
		default:
			fmt.Fprintln(os.Stderr, "Unknow return value of Wait()")
		}
	}
}

func followJournal() {
	err := doFollowJournal()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func init() {
	prometheus.MustRegister(sshCounter)
	prometheus.MustRegister(fingerprintUsage)
}

func main() {
	flag.Parse()

	go followJournal()

	http.Handle("/metrics", prometheus.Handler())
	http.ListenAndServe(*addr, nil)
}
